//
//  Groups.swift
//  StudyTwitter
//
//  Created by Schindar on 14.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
let groups = [Groups(id:1,groupName:"Einführung in die Prog.",shortCut:"EIP", imageName:"EIP"),
              Groups(id:2,groupName:"RechnerNetze" , shortCut:"RN", imageName:"RN") ,
              Groups(id:3,groupName:"RechnerArchitekture",shortCut:"RA", imageName:"RA"),
              Groups(id:4,groupName:"Test",shortCut:"Test", imageName:"RA"),
              Groups(id:5,groupName:"Test 2",shortCut:"Test2", imageName:"RA")]
struct Groups :Identifiable{
    var id :Int
    var groupName:String
    var shortCut : String
    var imageName: String
}

