//
//  Translation.swift
//  StudyTwitter
//
//  Created by admin on 07.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct Translation: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Translation_Previews: PreviewProvider {
    static var previews: some View {
        Translation()
    }
}
