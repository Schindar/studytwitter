//
//  CommentsUtilities.swift
//  StudyTwitter
//
//  Created by Ali on 18.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
import Firebase
class CommentsUtilities{
    static let db = Firestore.firestore()
    static let userId = Auth.auth().currentUser?.uid
    /**
    Set a comment into firestore.
    - Parameters :
    - msg: The  *text* of *post* wroten from user.
    - comment: The *comment* text from commentator.
    */
    static func setCommentData(comment: String,msg:String,postedDate :String,userId:String){
        db.collection("users").whereField("uid", isEqualTo: self.userId as Any)
        .getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    let commentator = document.get("firstname")
                    db.collection("comments").addDocument(data: ["comment":comment,"id":userId as Any,"commentator": commentator as Any,"msg":msg,"postedDate": postedDate,"userId":userId]){
                        (err)in
                        if err != nil{
                            print((err?.localizedDescription)!)
                            return
                        }
                        print("commented")
                    }
                }
            }
        }
    }

    /**
    Deletes a all comments from firestore according to their msg text.
    - Parameters :
    - msg: The *text* wroten from user.
    */
    static func deleteAllComments(msg :String){
        db.collection("comments").whereField("msg", isEqualTo: msg).getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    document.reference.delete()
                }
            }
        }
    }

    /**
    Deletes a comment from firestore.
    - Parameters :
    - commentator: The  *user* who wrote the comment
    - comment: the comment text
    */
    static func deleteSpecificComment(comment:String)  {
        db.collection("comments").whereField("comment", isEqualTo: comment).getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    let commentedUserId = document.get("id") as! String
                    if commentedUserId == userId{
                        document.reference.delete()
                    }
                }
            }
        }
    }

    /**
    Checks the Premession of deletion a comment from firestore.
    - Parameters :
    - comment: The  *comment* wroten user.
    */
    static func validateDeletionComment(comment:String,completion: @escaping (Bool)-> Void){
        db.collection("comments").whereField("comment", isEqualTo: comment).getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    let commentedUserId = document.get("id") as! String
                    if commentedUserId == userId{
                        completion(true)
                    }
                    else{
                        completion(false)
                    }
                }
            }
        }
    }

}

