//
//  MLConstanta.swift
//  StudyTwitter
//
//  Created by admin on 07.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

struct MLKitConstants{
    
    
    static let TEXT_RECOGNITION = "Text recognition"
    static let FACE_DETECTION = "Face detection"
    static let BARCODE_SCANNING = "Barcode scanning"
    static let IMAGE_LABELING = "Image labeling"
    static let OBJECT_DETECTION_TRACKING = "Object detection & tracking"
    static let LANDMARK_RECOGNITION = "Landmark recognition"
    static let LANGUAGE_IDENTIFICATION = "Language identification"
    static let TRANSLATION = "Translation"
    static let SMART_REPLAY = "Smart replay"
    static let AUTOML_MODEL_INFERENCE = "AutoML model inference"
    
    
}
