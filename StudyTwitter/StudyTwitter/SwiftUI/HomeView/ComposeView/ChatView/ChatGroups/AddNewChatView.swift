//
//  AddNewChatView.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI


struct AddNewChatView: View {
    
    @State private var chatRoomName = ""
    @State private var showCancelButton: Bool = false
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    
    var body: some View {
        Group{
            if(self.session.session != nil){
                VStack {
                    HStack{
                        
                        //    Image(systemName: "magnifyingglass")
                        
                        TextField("Chat Room Name",text: $chatRoomName, onEditingChanged: { isEditing in
                            self.showCancelButton = true
                        }, onCommit: {
                            print("onCommit")
                        }).foregroundColor(.black).lineSpacing(30)
                        
                        Button(action: {
                            self.chatRoomName=""
                        })
                        {
                            Image(systemName: "xmark.circle.fill").opacity(chatRoomName == "" ? 0 : 1)
                        }
                        
                        Button(action: {
                            
                        }) {
                            Text("Create Room")
                        }.padding(.all)
                            .background(Color.red)
                            .cornerRadius(16)
                            .foregroundColor(.white)
                            .font(Font.body.bold())
                        
                        
                    }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 200, alignment: .topLeading)
                    
                }
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}


struct AddNewChatView_Previews: PreviewProvider {
    static var previews: some View {
        AddNewChatView().environmentObject(SessionStore())
    }
}
