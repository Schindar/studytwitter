//
//  ComposeView.swift
//  StudyTwitter
//
//  Created by Schindar on 19.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
import Firebase
import FirebaseStorage
struct ComposeView : View {
    @Binding var show : Bool
    @State var txt = ""
    @State var maxOfCharacters = 100
    @EnvironmentObject var session: SessionStore
    @Environment(\.editMode) var mode
    @State var isCancelled = false
    @State var showingPicker = false
    @State private var showAlert = false
    @State var picker = false
    @State var picData : Data = .init(count:0)
    @State var loading = false
    var shortCut:String
    @State private var activeAlert: ValidatePostPosibilites = .EMPTY_TEXT
    var body : some View{
        VStack{
            HStack{
                Button(action: {
                    self.show.toggle()
                })
                {
                    Text("Cancel")
                }
                Spacer()
                if loading{
                    indicator()
                }
                else{
                    Button(action: {
                        self.picker.toggle()
                    })
                    {
                        Image(systemName: "photo.fill").resizable().frame(width:30,height: 25)
                    }
                    .foregroundColor(Color("bg"))
                }
                Text("(\(self.maxOfCharacters - self.txt.count))")
                Spacer()
                Button(action:{
                    if self.validatePost(text: self.txt, maxOfCharacters: self.maxOfCharacters) == ValidatePostPosibilites.EMPTY_TEXT.rawValue{
                        self.activeAlert = .EMPTY_TEXT
                    }
                    else if self.validatePost(text: self.txt, maxOfCharacters: self.maxOfCharacters) == ValidatePostPosibilites.HASHTAG_WITH_WHITESPACES.rawValue{
                        self.activeAlert = .HASHTAG_WITH_WHITESPACES
                    }
                    else if self.validatePost(text: self.txt, maxOfCharacters: self.maxOfCharacters) == ValidatePostPosibilites.OUTSIDE_THE_LIMIT.rawValue{
                        self.activeAlert = .OUTSIDE_THE_LIMIT
                    }
                    else if self.validatePost(text: self.txt, maxOfCharacters: self.maxOfCharacters) == ValidatePostPosibilites.ONLY_HASHTAG.rawValue{
                        self.activeAlert = .ONLY_HASHTAG
                    }
                    else if self.validatePost(text: self.txt, maxOfCharacters: self.maxOfCharacters) == "success" {
                        if !self.loading &&  self.picData.count != 0{
                            self.postWithPic()
                            self.loading.toggle()
                            return
                        }
                        if !self.loading{
                            PostUtilities.setPostData(msg: self.txt, shortCut: self.shortCut, pic: "")
                            self.show.toggle()
                        }
                    }
                    self.showAlert = true
                })
                {
                    Text("Post").padding()
                }
                .background(Color("bg"))
                .foregroundColor(.white)
                .clipShape(Capsule())
                .alert(isPresented: $showAlert) {
                    switch activeAlert{
                        case .EMPTY_TEXT:
                        return  Alert(title: Text("Important message"), message: Text("Empty posts are not allowed"), dismissButton: .default(Text("Got it!")))
                        case .HASHTAG_WITH_WHITESPACES:
                        return    Alert(title: Text("Important message"), message: Text("hashtags with spaces are not allowed in the text"), dismissButton: .default(Text("Got it!")))
                        case .OUTSIDE_THE_LIMIT:
                        return   Alert(title: Text("Important message"), message: Text("you typed in (\(txt.count)).Limit of text's characters are (\(maxOfCharacters))"), dismissButton: .default(Text("Got it!")))
                        case .ONLY_HASHTAG:
                        return   Alert(title: Text("Important message"), message: Text("Please write a world after (#) sign"), dismissButton: .default(Text("Got it!")))
                    }
                }
                Spacer()
            }
            multilineTextField(txt: $txt)
        }
        .sheet(isPresented: $picker){
            imagePicker(picker: self.$picker, picData:self.$picData)
        }
    }

    func postWithPic(){
        let storage = Storage.storage().reference()
        let id = "\(Int(Date().timeIntervalSince1970))"
        storage.child("pics").child(id).putData(self.picData,metadata:nil){
            (_,err) in
            if err != nil{
                print((err?.localizedDescription)!)
                return
            }
            storage.child("pics").child(id).downloadURL(){
                (url,err) in
                if err != nil{
                    print((err?.localizedDescription)!)
                    return
                }
                PostUtilities.setPostData(msg: self.txt, shortCut: self.shortCut, pic: "\(url!)")
                self.show.toggle()
            }
        }
    }

    func validatePost(text:String,maxOfCharacters:Int)->String{
        if text.trimmingCharacters(in: .whitespaces).isEmpty{
            return ValidatePostPosibilites.EMPTY_TEXT.rawValue
        }
        else if text.contains("# "){
            return ValidatePostPosibilites.HASHTAG_WITH_WHITESPACES.rawValue
        }
        else if text.count >= maxOfCharacters{
            return ValidatePostPosibilites.OUTSIDE_THE_LIMIT.rawValue
        }
        else if text.hasSuffix("#"){
            return ValidatePostPosibilites.ONLY_HASHTAG.rawValue
        }
        return "success"
    }

}

