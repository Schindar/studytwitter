//
//  Contacts.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct Contacts: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    
    var body: some View {
        
        Group{
            if(self.session.session != nil){
                VStack {
                    Text("Contacts")
                        .font(.largeTitle)
                        .fontWeight(.heavy)
                    
                }.padding(.horizontal, 20)
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

struct Contacts_Previews: PreviewProvider {
    static var previews: some View {
        Contacts().environmentObject(SessionStore())
    }
}
