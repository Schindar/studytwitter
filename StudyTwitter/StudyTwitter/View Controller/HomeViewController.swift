//
//  HomeViewController.swift
//  StudyTwitter
//
//  Created by admin on 08.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import UIKit
import SwiftUI

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var verticalStackViewMain: UIStackView!
    
    @EnvironmentObject var session: SessionStore
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        

                let homeView = HomeView().environmentObject(SessionStore())
                
                // This part below allows to use SwiftUI in UIKit
                let mainView = UIHostingController(rootView: homeView)
                self.addChild(mainView)
                mainView.view.frame = verticalStackViewMain.frame
                view.addSubview(mainView.view)
                mainView.didMove(toParent: self)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
