//
//  UserPosts.swift
//  StudyTwitter
//
//  Created by Schindar on 11.01.20.
//  Copyright © 2020 admin. All rights reserved.
//
import Foundation
struct UserPostsDataType : Identifiable {
    var id : String
    var msg : String
    var groupsShortcut : String
    var likes : String
    var postedDate: String
}


