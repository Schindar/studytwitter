//
//  UserAccountView.swift
//  StudyTwitter
//
//  Created by Schindar on 11.01.20.
//  Copyright © 2020 admin. All rights reserved.
//
import SwiftUI
struct UserAccountView: View {
    @EnvironmentObject var observedData : UsersPostsObservable
    var user = ""
    var email = ""
    var body: some View {
        ZStack{
            VStack{
                VStack(spacing : 5){
                    VStack {
                        Text(self.user)
                        .font(.title)
                        Text(self.email)
                        .font(.title)
                        Text("Bonus:")
                        .font(.subheadline)
                        Divider()
                    }
                    .padding()
                }
                VStack(spacing: 10){
                    List(observedData.UserPosts)
                    {
                        i in
                        UserAccountRow(msg:i.msg,postedDate: i.postedDate,email: self.email)
                    }.listStyle(GroupedListStyle())
                    .environment(\.horizontalSizeClass, .regular)
                }
            }
        }
    }

    struct UserAccountView_Previews: PreviewProvider {
        static var previews: some View {
            UserAccountView()
        }
    }

}

