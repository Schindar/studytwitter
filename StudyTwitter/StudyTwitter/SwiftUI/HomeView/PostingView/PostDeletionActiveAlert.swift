//
//  PostDeletionActiveAlert.swift
//  StudyTwitter
//
//  Created by Ali on 23.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
enum PostDeletionActiveAlert {
    case delete, denied
}

