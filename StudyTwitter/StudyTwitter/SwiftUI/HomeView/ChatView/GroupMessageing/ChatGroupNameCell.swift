//
//  ChatGroupNameCell.swift
//  StudyTwitter
//
//  Created by admin on 06.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct ChatGroupNameCell: View {
    @EnvironmentObject var session: SessionStore
    var chatGroup : ChatGroup
    func getUser(){
        self.session.listen()
    }
    var body: some View {
        HStack {
            Text(chatGroup.roomName)
                .bold()
                .foregroundColor(Color.black)
            
            /*.foregroundColor(Color.white)
             .padding(10)
             .background(Color(.red))
             .cornerRadius(20)
             */
            
        }
    }
}

struct ChatGroupNameCell_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ChatGroupNameCell(chatGroup: list[0])
                .previewLayout(.fixed(width: 300, height: 70))
            ChatGroupNameCell(chatGroup: list[1])
                .previewLayout(.fixed(width: 300, height: 70))
        }
    }
}
