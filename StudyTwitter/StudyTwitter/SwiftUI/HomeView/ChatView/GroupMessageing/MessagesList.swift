//
//  MessagesList.swift
//  StudyTwitter
//
//  Created by admin on 06.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct MessagesList: View {
    @EnvironmentObject var session: SessionStore
    func getUser(){
        self.session.listen()
    }
    var body: some View {
        Group{
            if(self.session.session != nil){
                List(list, id: \.id) { chatGroup in
                    
                    ChatGroupNameCell(chatGroup: chatGroup)
                }
                
                
                
            }else {
                AuthView()
            }
            
        }.onAppear(perform: getUser)
    }
}

struct MessagesList_Previews: PreviewProvider {
    static var previews: some View {
        MessagesList()
    }
}

let list = [ChatGroup(id: "1001",roomName:"dsfsf"),
            ChatGroup(id: "1002",roomName:"dsf"),
            ChatGroup(id: "1003",roomName:"SDad"),
            ChatGroup(id: "1004",roomName:"weq21"),
            ChatGroup(id: "1005",roomName:"45345gd")
    
]
