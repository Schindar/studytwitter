//
//  RoundRect.swift
//  PostingStudy
//
//  Created by admin on 01.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI
struct RoundRect: View {
    @State var flipped = false
    var body: some View {
        Capsule()
        .rotationEffect(.degrees(flipped ? -360 : 0), anchor: .center)
        .offset(y: 38)
        .frame(width: 50, height: 80)
        .opacity(0.4)
        .animation(Animation.linear(duration: 4.0).repeatForever(autoreverses: false).delay(3.0)).onAppear(){
            self.flipped.toggle()
        }
    }

}
struct RoundRect_Previews: PreviewProvider {
    static var previews: some View {
        RoundRect()
    }

}

