//
//  UserAccountRow.swift
//  StudyTwitter
//
//  Created by Schindar on 12.01.20.
//  Copyright © 2020 admin. All rights reserved.
//
import SwiftUI
struct UserAccountRow: View {
    var msg = ""
    var postedDate = ""
    var email = ""
    @State private var show_modal = false
    @State private var showingDeletionAlert = false
    var body: some View {
        VStack(alignment: .leading){
            HStack{
                HStack {
                    Text(self.msg).foregroundColor(.blue)
                    Spacer()
                    Text(self.postedDate).font(.subheadline).foregroundColor(.gray)
                }
                if self.email != ""{
                    Image("menu").resizable().frame(width: 15, height: 15)
                    .contextMenu {
                        Button(action: {
                            print("edit post")
                            self.show_modal.toggle()
                        })
                        {
                            Text("Edit post")
                            Image(systemName: "pencil")
                        }
                        .sheet(isPresented: self.$show_modal) {
                            ComposeView(show: self.$show_modal, shortCut: "")
                        }
                        Button(action: {
                            self.showingDeletionAlert = true
                        })
                        {
                            Text("Delete post")
                            Image(systemName: "xmark.square.fill")
                        }
                        .alert(isPresented:self.$showingDeletionAlert) {
                            Alert(title: Text("Are you sure you want to delete this?"), message: Text("There is no undo"), primaryButton: .destructive(Text("Delete")) {
                                print("Deleting...")
                                PostUtilities.deletePost(msg: self.msg, postedDate: self.postedDate)
                            }
                                  , secondaryButton: .cancel())
                        }
                    }
                }
            }
            .padding(.horizontal)
        }
    }

}
struct UserAccountRow_Previews: PreviewProvider {
    static var previews: some View {
        UserAccountRow()
    }

}

