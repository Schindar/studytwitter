//
//  UsersPostsObservable.swift
//  StudyTwitter
//
//  Created by Schindar on 11.01.20.
//  Copyright © 2020 admin. All rights reserved.
//
import SwiftUI
import Firebase
class UsersPostsObservable : ObservableObject{
    @Published var UserPosts = [UserPostsDataType]()
    init(userId:String) {
        let db = Firestore.firestore()
        db.collection("posts").whereField("id", isEqualTo: userId).addSnapshotListener {
            (snap, err) in
            if err != nil{
                print((err?.localizedDescription)!)
                return
            }
            for i in snap!.documentChanges{
                // If there is any additional information in post document in *FireStore*
                if i.type == .added{
                    let id = i.document.documentID
                    let msg = i.document.get("msg") as! String
                    let likes = i.document.get("likes") as! String
                    let groupsShortcut = i.document.get("groupsShortcut") as! String
                    let postedDate = i.document.get("postedDate") as! String
                    DispatchQueue.main.async {
                        self.UserPosts.append(UserPostsDataType(id: id, msg: msg, groupsShortcut: groupsShortcut,likes:likes, postedDate: postedDate))
                    }
                }
                // If there is any documentation removed from the user.
                if i.type == .removed{
                    let id = i.document.documentID
                    for j in 0..<self.UserPosts.count{
                        if self.UserPosts[j].id == id{
                            self.UserPosts.remove(at: j)
                            return
                        }
                    }
                }
            }
        }
    }

}

