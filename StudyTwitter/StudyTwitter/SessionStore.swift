//
//  SessionStore.swift
//  PostingStudy
//
//  Created by admin on 30.11.19.
//  Copyright © 2019 admin. All rights reserved.
//

import SwiftUI
import Firebase
import Combine

class SessionStore: ObservableObject{
    
    var didChange = PassthroughSubject<SessionStore, Never>()
    @Published var session: User? {didSet{self.didChange.send(self)}}
    var handle: AuthStateDidChangeListenerHandle?
    // monitor authentication changes using firebase
    func listen(){
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if let user = user {
                // if we have a user, create a new user model
                print("Got user: \(String(user.email!))")
                self.session = User(uid: user.uid, email: user.email, firstName: "", lastName:  "")
            } else {
                // if we don't have a user, set our session to nil
                print("Dont got a user!")
                self.session = nil
            }
        })
    }
    
    
    func signUp(email: String, password: String, handler: @escaping AuthDataResultCallback){
        Auth.auth().createUser(withEmail: email, password: password, completion: handler)
    }
    
    func signIn(email: String, password: String, handler: @escaping AuthDataResultCallback){
        Auth.auth().signIn(withEmail: email, password: password, completion: handler)
    }
    
    func signOut(){
        do {
            try Auth.auth().signOut()
            self.session = nil
            print("Successfully singing out!")
        } catch {
            print("Error signing out!")
        }
    }
    
    func unbind() {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
    
    deinit {
        unbind()
    }
    
}

