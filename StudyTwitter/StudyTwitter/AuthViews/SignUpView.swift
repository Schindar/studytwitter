//
//  SignUpView.swift
//  PostingStudy
//
//  Created by admin on 30.11.19.
//  Copyright © 2019 admin. All rights reserved.
//

import SwiftUI
import Firebase


struct SignUpView: View {
    
    
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var email: String = ""
    @State var bonus: String = ""
    @State var password: String = ""
    @State var conformPassword: String = ""
    @State var error: String = ""
    @State var image: Image? = nil
    @State var showingPicker = false
    @State var isCancelled = false
    @EnvironmentObject var session: SessionStore
    
    
    func validateFields() -> String? {
        
        if (self.email.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            self.password.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            self.firstName.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            self.lastName.trimmingCharacters(in: .whitespacesAndNewlines) == "") {
            return "Please fill are text fields"
        }
        
        if self.isValidEmail(emailStr: self.email) == false {
            return "Email don't contains the @campus.lmu.de"
        }
        
        let cleanedPassword = self.password.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            return "Please make sure your password is at leat 8 characters, containts a special character and  a number."
        }
        
        return nil
    }
    
    /**
     Check if the email is valid.
     @Return {true} if the email is valid, else false.
     */
    func isValidEmail(emailStr:String) -> Bool {
        if((emailStr.contains("@campus.lmu.de"))){
            return true
        }
        return false
    }
    
    func signUp(){
        
        let err = self.validateFields()
        
        if err != nil {
            error = err!
        } else {
            
            self.session.signUp(email: email, password: password){(result, error) in
                if let error = error {
                    self.error = error.localizedDescription
                } else {
                    
                    let db = Firestore.firestore()
                    guard let userID = result?.user.uid
                        else {
                            return
                    }
                    
                    db.collection("users").document(userID).setData( ["firstname":self.firstName, "lastname":self.lastName, "uid": result!.user.uid, "email": self.email,"bonus":self.bonus ]) { (error) in
                        
                    }
                    self.email = ""
                    self.password = ""
                }
            }
        }
    }
    
    var body: some View {
        GeometryReader{ geometry in
            VStack{
                Text("Create Account")
                    .font(.system(size: 32, weight: .heavy))
                
                Text("Sign up to get started")
                    .font(.system(size: 18, weight: .medium))
                    .foregroundColor(Color.black)
                
                VStack(spacing: 18){
                    
                    
                    Button(action: {
                        self.showingPicker = true
                    }) {
                        
                        Image(systemName: Constants.personCircleFill)
                            .imageScale(.large)
                            .foregroundColor(.white)
                        
                    }
                    
                    self.image?
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 150, height: 150)
                        .clipped()
                        .cornerRadius(150)
                        .padding(.bottom, 5)
                    
                    TextField("First name", text: self.$firstName)
                        .font(.system(size: 14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                        .foregroundColor(Color.white)
                    
                    TextField("Last name", text: self.$lastName)
                        .font(.system(size: 14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                        .foregroundColor(Color.white)
                    
                    
                    TextField("Email adress", text: self.$email)
                        .font(.system(size: 14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                        .foregroundColor(Color.white)
                    
                    
                    
                    SecureField("Password", text: self.$password)
                        .font(.system(size: 14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                        .foregroundColor(Color.white)
                    
                }.padding(.vertical, 64)
                    .sheet(isPresented: self.$showingPicker,
                           onDismiss: {
                            // do whatever you need here
                    }, content: {
                        ImagePicker.shared.view
                    })
                    .onReceive(ImagePicker.shared.$image) { image in
                        // This gets called when the image is picked.
                        // sheet/onDismiss gets called when the picker completely leaves the screen
                        self.image = image
                }
                
                
                Button(action: self.signUp){
                    Text("Create Account")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .foregroundColor(.white)
                        .font(.system(size: 14, weight: .bold))
                        .background(LinearGradient(gradient: Gradient(colors: [Color.green, Color.blue]), startPoint: .leading, endPoint: .trailing))
                        .cornerRadius(5)
                }
                
                if (self.error != ""){
                    Text(self.error)
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(.white)
                        .padding()
                }
                
                Spacer()
            }.padding(.horizontal, 32)
                .background(
                      RadialGradient(gradient: Gradient(colors: [Color.blue, Color.black]), center: .center, startRadius: 5, endRadius: 600).edgesIgnoringSafeArea(.all)
            )
        }
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView().environmentObject(SessionStore())
    }
}

