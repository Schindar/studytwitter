//
//  AccountView.swift
//  StudyTwitter
//
//  Created by admin on 13.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI
import FirebaseAuth

struct AccountView: View {
    
    @EnvironmentObject var session: SessionStore
    
    @Environment(\.editMode) var mode
    
    @State var isCancelled = false
    
    @State var showingPicker = false
    
    @State var image : Image? = nil
    // you could also use ImagePicker.shared.image directly
    
    var body: some View {
        
        NavigationView{
            Group {
                if self.session.session != nil {
                    ScrollView{
                        VStack(alignment: .leading, spacing: 5){
                            HStack(alignment: .top, spacing: 5){
                                Button(action: {
                                    self.showingPicker = true
                                }) {
                                    
                                    Image(systemName: "square.and.arrow.up").font(.largeTitle).foregroundColor(.blue)
                                }
                                
                                image?
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 300)
                                    .clipShape(Circle())
                                
                            }.sheet(isPresented: $showingPicker,
                                    onDismiss: {
                                        // do whatever you need here
                            }, content: {
                                ImagePicker.shared.view
                            })
                                .onReceive(ImagePicker.shared.$image) { image in
                                    // This gets called when the image is picked.
                                    // sheet/onDismiss gets called when the picker completely leaves the screen
                                    self.image = image
                            }
                        }
                        Divider()
                        HStack{
                            VStack{
                                Text("session.session.email")
                            }
                        }
                        Divider()
                        HStack{
                            Text("Addvanced såetting")
                        }
                        Spacer()
                    }.navigationBarItems(trailing: Button(action:{
                        self.editButtonTapped(self)
                    }){
                        Image(systemName: "pencil.tip.crop.circle.badge.plus").font(.largeTitle).foregroundColor(.blue)
                    })
                }
                    
                    
                    
                else {
                    Text("Our authentication screen goes ")
                }
            }.onAppear(perform: getUser)
        }
    }
    
    func editButtonTapped(_ sender: Any){
        print("test")
    }
    
    func getUser(){
        session.listen()
    }
    
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView().environmentObject(SessionStore())
    }
}
