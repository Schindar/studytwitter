//
//  HomeRow.swift
//  StudyTwitter
//
//  Created by admin on 11.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

// content the image in the home view. After the user choosing one of the image, it navigate to the view image.
struct HomeRow: View {
    
    var categoryName:String
    var homes:[Home]
    
    var body: some View {
        VStack(){
            Text(self.categoryName)
            .font(.title)
            
            ScrollView(.horizontal, showsIndicators: false){
                HStack(alignment: .top){
                    ForEach(self.homes, id: \.self) { home in
                        Group {
                                if home.view == Constants.Storyboard.accountView {
                                    NavigationLink(destination: AccountView()) {
                                        HomeItem(home: home)
                                            .frame(width: 300)
                                            .padding(.trailing, 30)
                                    }
                                } else if home.view == Constants.Storyboard.searchView {
                                    NavigationLink(destination: SearchView()){
                                        HomeItem(home: home)
                                            .frame(width: 300)
                                            .padding(.trailing, 30)
                                    }
                                } else if home.view == Constants.Storyboard.groupsView {
                                    NavigationLink(destination: GroupsView()){
                                        HomeItem(home: home)
                                            .frame(width: 300)
                                            .padding(.trailing, 30)
                                    }
                                } else if home.view == Constants.Storyboard.chatView {
                                    NavigationLink(destination: ChatView()) {
                                        HomeItem(home: home)
                                            .frame(width: 300)
                                            .padding(.trailing, 30)
                                    }
                            }
                        }
                    }
                }
            }
        }
    }
}

struct HomeRow_Previews: PreviewProvider {
    static var previews: some View {
        HomeRow(categoryName: "intern", homes: homeData)
    }
}
