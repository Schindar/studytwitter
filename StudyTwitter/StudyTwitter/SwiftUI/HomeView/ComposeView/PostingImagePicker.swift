//
//  PostingImagePicker.swift
//  StudyTwitter
//
//  Created by Schindar on 04.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
import SwiftUI
struct imagePicker:UIViewControllerRepresentable {
    @Binding var picker : Bool
    @Binding var picData : Data
    func makeCoordinator() -> imagePicker.Coordinator {
        return imagePicker.Coordinator(parent1:self)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<imagePicker>) -> UIImagePickerController {
        let picker1 = UIImagePickerController()
        picker1.sourceType = .photoLibrary
        picker1.delegate = context.coordinator
        return picker1
    }

    func updateUIViewController(_ uiViewController:UIImagePickerController, context: UIViewControllerRepresentableContext<imagePicker>) {
    }

    class Coordinator : NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
        var parent : imagePicker
        init(parent1:imagePicker){
            parent = parent1
        }
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let image = info[.originalImage] as! UIImage
            let picdata = image.jpegData(compressionQuality: 0.25)
            self.parent.picData = picdata!
            self.parent.picker.toggle()
        }
    }

}

