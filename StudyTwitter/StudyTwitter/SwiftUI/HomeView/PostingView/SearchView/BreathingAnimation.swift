//
//  BreathingAnimation.swift
//  StudyTwitter
//
//  Created by admin on 10.01.20.
//  Copyright © 2020 admin. All rights reserved.
//


import SwiftUI

struct BreathingAnimation: View {
    
    @State private var scale_in_out = false
    @State private var rotate_in_out = false
    @State private var rotateOuter = false
    var color: Color
    
    var body: some View {
        ZStack{
            Group{
                ZStack{
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(color)
                        .offset(y: -42)
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(color)
                        .offset(y: 42)
                }
            }.opacity(1/3)
            
            Group{
                ZStack{
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(color)
                        .offset(y: -42)
                    
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(color)
                        .offset(y: 42)
                }.rotationEffect(.degrees(self.rotateOuter ? 360*3 : 0))
                    .animation(Animation.spring(response: 0.87,
                                                dampingFraction: 0.1, blendDuration: 0.3)
                        .repeatForever(autoreverses: true))
            }.rotationEffect(.degrees(40))
                .opacity(1/4)
            
            Group{
                ZStack{
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(color)
                        .offset(y: -42)
                    
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(color)
                        .offset(y: 42)
                }
            }.rotationEffect(.degrees(120))
                .opacity(1/2)
        }.rotationEffect(.degrees(self.rotate_in_out ? 90 : 0))
            .scaleEffect(self.scale_in_out ? 1 : 1/8)
            .animation(Animation.easeInOut.repeatForever(autoreverses: true).speed(1/8))
            .onAppear(){
                self.rotate_in_out.toggle()
                self.scale_in_out.toggle()
        }
    }
}

struct BreathingAnimation_Previews: PreviewProvider {
    static var previews: some View {
        BreathingAnimation(color: Color.blue)
    }
}
