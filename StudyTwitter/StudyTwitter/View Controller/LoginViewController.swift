//
//  LoginViewController.swift
//  StudyTwitter
//
//  Created by admin on 08.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @Published  var session = SessionStore()
    
    @IBOutlet weak var backToEntryButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initElements()
    }
    
    /**
     Style the view component in this class.
     */
    func initElements(){
        
        // Hide the error label
        errorLabel.alpha = 0
        
        // Style the text fields
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        
        // Style the button
        Utilities.styleFilledButton(loginButton)
        Utilities.styleHollowButton(backToEntryButton)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    /**
     This function is called when the user click the login.
     When the authenification is right the user will link to the main view (home view).
     */
    @IBAction func loginTapped(_ sender: Any) {
        
        //check if all fields are not empty
        let error = validateFields()
        
        // If is != nil then the fields are wrong, show error message
        if error != nil {
            showError(error!)
        } else {
            //try to login in with the entered email and password
            
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            // authentication with the entered email and password
            /*Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error != nil {
                    //User can't sign in
                    self.errorLabel.text = error!.localizedDescription
                    self.errorLabel.alpha = 1
                }
                else {
                    // Transition to the home screen
                    self.enableTransitionMainScreen()
                }
            }
            */
            
            session.signIn(email: email, password: password, handler: { resut, error in
                if error != nil {
                    self.errorLabel.text = error!.localizedDescription
                    self.errorLabel.alpha = 1
                }else {
                    self.enableTransitionMainScreen()
                }
            })
            
        }
    }
    
    /**
     This function check the text fields that are defined in this class.
     f the values are correct, it returns a nil else the error message.
     */
    func validateFields() -> String? {
        
        // It checks if all fields are filled in.
        if  emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill all fields."
        }
        return nil
    }
    
    /**
     Function to show error message to the user.
     */
    func showError(_ message:String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    /**
     Redirect to the home view (main view of this application).
     */
    func enableTransitionMainScreen(){
        let homeViewController =
            storyboard?.instantiateViewController(identifier:
                Constants.Storyboard.homeViewController) as?
        HomeViewController
        
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    }
    
}
