//
//  User.swift
//  StudyTwitter
//
//  Created by admin on 14.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

class User {
    var uid: String
    var email: String?
    var displayName: String?


    init(uid: String, displayName: String?, email: String?) {
        self.uid = uid
        self.email = email
        self.displayName = displayName
    }

}
