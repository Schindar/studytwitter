//
//  ViewController.swift
//  StudyTwitter
//
//  Created by Schindar on 02.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initElements()
    }
   
    /**
     Style the component of this class.
     */
    func initElements(){
        // Style the button
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(loginButton)
    }
    

}

