//
//  SignInView.swift
//  PostingStudy
//
//  Created by admin on 30.11.19.
//  Copyright © 2019 admin. All rights reserved.
//

import SwiftUI

struct SignInView: View {
    
    @State var email: String = ""
    @State var password: String = ""
    @State var error: String = ""
    @EnvironmentObject var session: SessionStore
    
    
    func validateFields() -> String? {
        if self.email.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            self.password.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill all fields, thank you!"
        }
        return nil
    }
    
    
    func signIn(){
        
        let error = self.validateFields()
        
        if error != nil {
            self.error = error!
        } else {
            
            self.email = self.email.trimmingCharacters(in: .whitespacesAndNewlines)
            self.password = self.password.trimmingCharacters(in: .whitespacesAndNewlines)
            
            session.signIn(email: email, password: password) {(result, error) in
                if let error = error {
                    self.error = error.localizedDescription
                } else {
                    self.email = ""
                    self.password = ""
                }
            }
        }
    }
    
    var body: some View {
        GeometryReader{ geometry in
            VStack{
                
                Text("Posting LMU")
                .font(.system(size: 20, weight: .medium))
                .foregroundColor(.black)
                
                Text("Sign in to continue")
                    .font(.system(size: 16, weight: .medium))
                    .foregroundColor(.black)
             
                
                VStack(spacing: 18){
                    
  
                TextField("Email adress", text: self.$email)
                    .font(.system(size: 14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                    .foregroundColor(Color.white)
                        
                    
                    SecureField("Password", text: self.$password)
                        .font(.system(size: 14))
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                        .foregroundColor(Color.white)
                                              
                }
                .padding(.vertical, 64)
                
                Button(action: self.signIn){
                    Text("Sign in")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .foregroundColor(.white)
                        .font(.system(size: 14, weight: .bold))
                        .background(LinearGradient(gradient: Gradient(colors:[Color.green, Color.blue]), startPoint: .leading, endPoint: .trailing))
                        .cornerRadius(5)
                }
                
                
                if self.error != "" {
                    Text(self.error)
                        .font(.system(size: 14, weight: .bold))
                        .foregroundColor(.white)
                        .padding()
                }
                
                Spacer()
                
                BreathingAnimation(color: Color.green)
                Spacer()
                
                NavigationLink(destination: SignUpView()){
                    HStack{
                        Text("I'm a new user.")
                            .font(.system(size: 14, weight: .light))
                            .foregroundColor(Color.white)
                        
                        Text("Create an account")
                            .font(.system(size: 14, weight: .bold))
                            .foregroundColor(Color.white)
                    }
                }
                
            }
            .padding(.horizontal, 32)
            .background(
                RadialGradient(gradient: Gradient(colors: [Color.blue, Color.black]), center: .center, startRadius: 5, endRadius: 600).edgesIgnoringSafeArea(.all)
            )
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}

