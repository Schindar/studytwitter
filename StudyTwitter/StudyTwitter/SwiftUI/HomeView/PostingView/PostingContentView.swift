//
//  ContentView.swift
//  StudyTwitter
//
//  Created by Schindar on 21.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
struct PostingContentView: View {
    @State private var searchText = ""
    @State private var showCancelButton: Bool = false
    var shortCut:String
    @State private var show_modal = false
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
            VStack {
                HStack {
                    HStack {
                        Image(systemName: "magnifyingglass")
                        TextField("search", text: $searchText, onEditingChanged: {
                            isEditing in
                            self.showCancelButton = true
                        }
                                  , onCommit: {
                                      print("onCommit")
                                  })
                        .foregroundColor(.primary)
                        Button(action: {
                            self.searchText = ""
                        })
                        {
                            Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                        }
                    }
                    .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
                    .foregroundColor(.secondary)
                    .background(Color(.secondarySystemBackground))
                    .cornerRadius(10.0)
                    if showCancelButton  {
                        Button("Cancel") {
                            UIApplication.shared.endEditing(true) // this must be placed before the other commands here
                            self.searchText = ""
                            self.showCancelButton = false
                        }
                        .foregroundColor(Color(.systemBlue))
                    }
                }
                .padding(.horizontal)
                .navigationBarHidden(showCancelButton)
                PostView(shortcut : self.shortCut,searchText :self.searchText).environmentObject(getPostData(shortCut:self.shortCut))
            } .navigationBarTitle("Group:\(shortCut)")
                   .resignKeyboardOnDragGesture()
                   .navigationBarItems(trailing:
                                       Button(action: {
                                           self.show_modal.toggle()
                                       })
                                       {
                                           Image(systemName: "square.and.pencil")
                                       }
                                       .sheet(isPresented: self.$show_modal) {
                                           ComposeView(show: self.$show_modal, shortCut: self.shortCut)
                                       }
                   )
        }
       
    }


struct PostingContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PostingContentView(shortCut: "EIP")
            .environment(\.colorScheme, .light)
            PostingContentView(shortCut: "EIP")
            .environment(\.colorScheme, .dark)
        }
    }

}
extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
        .filter{
            $0.isKeyWindow
        }
        .first?
        .endEditing(force)
    }

}
struct ResignKeyboardOnDragGesture: ViewModifier {
    var gesture = DragGesture().onChanged{
        _ in
        UIApplication.shared.endEditing(true)
    }

    func body(content: Content) -> some View {
        content.gesture(gesture)
    }

}
extension View {
    func resignKeyboardOnDragGesture() -> some View {
        return modifier(ResignKeyboardOnDragGesture())
    }

}

