import SwiftUI
import SDWebImageSwiftUI
import Firebase
struct PostCellCard : View {
    var userId = ""
    var user = ""
    var image = ""
    var id = ""
    var likes = ""
    var postedDate = ""
    var msg = ""
    @State private var comment: String = ""
    @State private var show_modal = false
    @ObservedObject var ObserComment = observerComments(userId: "", postedDate: "")
    var body : some View{
        NavigationLink(destination: CommentContent(userId:self.userId,UserName: self.user,msg: self.msg, image: self.image,likes:self.likes,id :self.id,postedDate: self.postedDate).environmentObject(observerComments(userId: self.userId, postedDate: self.postedDate))) {
            VStack(alignment: .leading) {
                Text(user).font(.title)
                HStack {
                    Text(self.msg).font(.subheadline).foregroundColor(.blue)
                    Spacer()
                    if self.image != ""{
                        Image(systemName: "paperclip").font(.body).foregroundColor(.black)
                    }
                    Text(self.postedDate).font(.subheadline).foregroundColor(.gray)
                }
            }
        }
    }

}
struct PostCellCard_Previews: PreviewProvider {
    static var previews: some View {
        PostCellCard()
    }

}

