//
//  multilineTextField.swift
//  Twitter
//
//  Created by Kavsoft on 26/11/19.
//  Copyright © 2019 Kavsoft. All rights reserved.
//

import SwiftUI

// now we going to create multiline Textfield.....

struct multilineTextField : UIViewRepresentable {
    
    
    @Binding var txt : String
    
    func makeCoordinator() -> multilineTextField.Coordinator {
        
        return multilineTextField.Coordinator(parent1 : self)
    }
    func makeUIView(context: UIViewRepresentableContext<multilineTextField>) -> UITextView {
        
        let text = UITextView()
        let borderGray = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1)
        
        text.layer.borderColor = borderGray.cgColor
        text.layer.borderWidth = 4.0;
        text.layer.cornerRadius = 25.0;
        text.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: 10, height: 10))
        text.isEditable = true
        text.isUserInteractionEnabled = true
        text.text = "Type Something"
        text.textColor = UIColor.lightGray
        text.font = .systemFont(ofSize: 20)
        text.delegate = context.coordinator
        return text
    }
    
    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<multilineTextField>) {
        
        if self.txt != "" {
            uiView.text = self.txt
        }
    }
    
    class Coordinator : NSObject,UITextViewDelegate{
        
        
        var parent : multilineTextField
        
        init(parent1 : multilineTextField) {
            
            parent = parent1
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            
            textView.text = ""
            textView.textColor = .black
            
        }
        
        func textViewDidChange(_ textView: UITextView) {
            
            self.parent.txt = textView.text
        }
    }
}
