//
//  CellView.swift
//  StudyTwitter
//
//  Created by Ali on 15.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
import SwiftUI
struct CellView : View {
    var edit : Bool
    var data : datatypeOfComments
    @State private var showingDeletionCommentAlert = false
    @State private var activePremessionAlert: CommentDeletionActiveAlert = .denied
    var body : some View{
        HStack{
            if edit{
                Button(action: {
                    CommentsUtilities.validateDeletionComment(comment: self.data.comment) {
                        flag in
                        print(flag)
                        if flag {
                            self.activePremessionAlert = .delete
                        }
                        else {
                            self.activePremessionAlert = .denied
                        }
                    }
                    self.showingDeletionCommentAlert = true
                })
                {
                    Image(systemName: "minus.circle").font(.title)
                }
                .foregroundColor(.red)
                .alert(isPresented:$showingDeletionCommentAlert) {
                    switch activePremessionAlert {
                        case .delete:
                        return   Alert(title: Text("Are you sure you want to delete this?"), message: Text("There is no undo"), primaryButton: .destructive(Text("Delete")) {
                            CommentsUtilities.deleteSpecificComment(comment: self.data.comment)
                        }
                                       , secondaryButton: .cancel())
                        case .denied:
                        return Alert(title: Text("Important message"), message: Text("you don't have permission to delete this"))
                    }
                }
            }
            Button(action: {
            })
            {
                Image(systemName: "checkmark.rectangle").foregroundColor(.yellow)
            }
            .foregroundColor(.red)
            Text(data.commentator).lineLimit(1)
            Spacer()
            VStack(alignment: .leading,spacing : 5){
                Text(data.comment)
            }
        }
        .padding()
        .background(RoundedRectangle(cornerRadius: 25).fill(Color.white))
        .animation(.spring())
    }

}

