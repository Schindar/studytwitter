//
//  GroupsContentRow.swift
//  StudyTwitter
//
//  Created by Schindar on 14.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
struct GroupsContentRow: View {
    var groupName: String
    var shortCut:String
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(groupName)
                .font(.title)
                Text(shortCut)
            }
        }
    }

}
struct GroupsContentRow_Previews: PreviewProvider {
    static var previews: some View {
        GroupsContentRow(groupName: "Einführung in die Prog.", shortCut: "EIP")
    }

}

