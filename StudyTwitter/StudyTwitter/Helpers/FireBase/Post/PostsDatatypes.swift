//
//  PostsDatatypes.swift
//  StudyTwitter
//
//  Created by Ali on 18.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
struct postsDatatype : Identifiable {
    var id : String
    var name : String
    var msg : String
    var groupsShortcut : String
    var likes : String
    var postedData: String
    var userId : String
    var pic : String
}

