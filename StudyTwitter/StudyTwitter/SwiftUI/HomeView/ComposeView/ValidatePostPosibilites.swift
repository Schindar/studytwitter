//
//  Enums.swift
//  StudyTwitter
//
//  Created by Ali on 18.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
enum ValidatePostPosibilites :String{
    case EMPTY_TEXT = "emptyText"
    case HASHTAG_WITH_WHITESPACES = "hashtagsWithWhiteSpaces"
    case OUTSIDE_THE_LIMIT = "outSideTheLimit"
    case ONLY_HASHTAG = "onlyHashtag"
}

