//
//  ProfileBackground.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct ProfileBackground: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        Group{
            if(self.session.session != nil){
                Text("Hallo")
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

struct ProfileBackground_Previews: PreviewProvider {
    static var previews: some View {
        ProfileBackground().environmentObject(SessionStore())
    }
}
