//
//  SignUpViewController.swift
//  StudyTwitter
//
//  Created by admin on 08.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
class SignUpViewController: UIViewController {
    
    @Published  var session = SessionStore()
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var backToEntryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initElements()
    }
    
    /**
     This function style the component of this class.
     */
    func initElements(){
        
        // Hide the error label
        errorLabel.alpha = 0
        
        // Style the text field elements
        Utilities.styleTextField(firstNameTextField)
        Utilities.styleTextField(lastNameTextField)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        
        // Style the button element
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(backToEntryButton)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // This function check the text fields that are defined in this class.
    // If the values are correct, it returns a nil else the error message.
    func validateFields() -> String? {
        
        // It checks if all fields are filled in.
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill are text fields."
        }
        
        // Check if the password is secure and correct
        let cleanedPasswordField = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPasswordField) == false {
            // Password is wrong and not secure enough
            return "Please make sure you password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    
    /**
     This function is triggered when the user click on the sign up botton.
     It check the value of the text fields and so on.
     */
    @IBAction func signUpTapped(_ sender: Any) {
        
        //enableTransitionMainScreen()
        
        // Validate the fields
        let error = validateFields()
        
        // If is != nil then the fields are wrong, show error message
        if error != nil {
            showError(error!)
        } else {
            
            // Cleaned data of the textfield
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            if(self.isValidEmail(emailStr: email)){
                session.signUp(email: email, password: password, handler: { result, error in
                    
                  if error != nil {
                                           //check wethere the email is already in use by another account.
                                           self.emailIsInUse(emailStr: email)
                                       }else {
                                           // User was created successfully, now store the first name and last name
                                           let db = Firestore.firestore()
                                           guard let userID = result?.user.uid
                                               else {
                                                   return
                                           }
                                           
                                           db.collection("users").document(userID).setData( ["firstname":firstName, "lastname":lastName, "uid": result!.user.uid ]) { (error) in
                                               if error != nil {
                                                   // Show error message
                                                   self.showError("Error saving user data")
                                               }
                                           }
                                           
                                           /*db.collection("users").addDocument(data: ["firstname":firstName, "lastname":lastName, "uid": result!.user.uid ]) { (error) in
                                               if error != nil {
                                                   // Show error message
                                                   self.showError("Error saving user data")
                                               }
                                           }*/
                                           // Transition to the home screen
                                           self.enableTransitionMainScreen()
                                       }
                })
            } else{
                self.alert(alertStr:  "plaese use your campus account. @campus.lmu.de")
            }
        }
    }
    
    /**
     Redirect to the home view (main view of this application).
     */
    func enableTransitionMainScreen(){
        let homeViewController =
            storyboard?.instantiateViewController(identifier:
                Constants.Storyboard.homeViewController) as?
        HomeViewController
        
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    }
    
    /**
     Function to show error message to the user.
     */
    func showError(_ message:String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    /**
     Check if the email is valid.
     @Return {true} if the email is valid, else false.
     */
    func isValidEmail(emailStr:String) -> Bool {
        if((emailStr.contains("@campus.lmu.de"))){
            return true
        }
        return false
    }
    /**
     Check if the email not in use.
     @Return {true} if the email is not in use, else false.
     */
    func emailIsInUse(emailStr:String) {
        Auth.auth().fetchSignInMethods(forEmail: emailStr, completion: {
            (providers, error) in
            if let error = error {
                print(error.localizedDescription)
            } else if providers != nil {
                self.alert(alertStr:  "The email address is already in use by another account.")
            }
        })
        
    }
    /**
     Show Alert to the user.
     */
    
    func alert(alertStr:String){
        let alert = UIAlertController(title: "Alert", message: alertStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}
