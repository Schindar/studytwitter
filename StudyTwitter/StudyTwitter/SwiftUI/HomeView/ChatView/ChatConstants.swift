//
//  ChatConstants.swift
//  StudyTwitter
//
//  Created by admin on 05.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import UIKit

struct ChatConstants {
    
    struct TabBarImageName {
        static let tabBar0 = "message.fill"
        static let tabBar1 = "person.fill"
        static let tabBar2 = "text.justify"
        static let tabBar3 = "cart.fill"
    }
    
    struct TabBarText {
        static let tabBar0 = "Chats"
        static let tabBar1 = "Groups"
        static let tabBar2 = "Contacts"
        static let tabBar3 = "Requests"
    }
}
struct SFSymbolName {
    static let play_circle_fill = "play.circle.fill"
    static let suit_heart_fill = "suit.heart.fill"
    static let person_circle_fill = "person.circle.fill"
    static let person_fill = "person.fill"
}

struct ChatGroup:Identifiable{
    var id: String
    var roomName: String
    
}

struct Message:Identifiable{
    var id: String
    var messageText: String
    var senderId: String
    var senderName: String?
    let date: Date
    
}
