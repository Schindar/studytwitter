//
//  PostUtilities.swift
//  StudyTwitter
//
//  Created by Ali on 18.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
import Firebase
class PostUtilities{
    static let db = Firestore.firestore()
    static let userId = Auth.auth().currentUser?.uid
    /**
    Set a post into firestore.
    - Parameters :
    - msg: The  *text* wroten from user.
    - shortCut:  The *short cut*of the group
    - pic: The uploaded image from user.
    */
    static func setPostData(msg : String,shortCut:String,pic:String){
     
        self.db.collection("users").whereField("uid", isEqualTo: userId as Any)
        .getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    let name = document.get("firstname")
                    db.collection("posts").document().setData(["name" : name as Any,"id":userId as Any,"msg":msg,"groupsShortcut":shortCut,"postedDate":Utilities.getCurrentDate() ,"likes":"0","pic":pic]) {
                        (err) in
                        if err != nil{
                            print((err?.localizedDescription)!)
                            return
                        }
                        print("success")
                    }
                }
            }
        }
    }

    /**
    Deletes a post from firestore.
    - Parameters :
    - msg: The  *text* wroten user.
     -postedDate: the date where the user posted his queaston.
    */
    static func deletePost(msg :String,postedDate:String){
        self.db.collection("posts").whereField("id", isEqualTo: userId as Any).getDocuments() {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                for document in querySnapshot!.documents {
                    let postedMsg = document.get("msg") as! String
                    let postedDateFireStore = document.get("postedDate") as! String
                    if postedMsg == msg && postedDateFireStore == postedDate {
                        CommentsUtilities.deleteAllComments(msg: msg)
                        document.reference.delete()
                    }
                }
            }
        }
    }
}

