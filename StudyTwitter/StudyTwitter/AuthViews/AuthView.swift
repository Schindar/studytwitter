//
//  AuthView.swift
//  PostingStudy
//
//  Created by admin on 30.11.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI
struct AuthView: View {
    var body: some View {
        NavigationView{
            SignInView()
        }.environment(\.horizontalSizeClass, .compact)

    }

}
struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView().environmentObject(SessionStore())
    }

}

