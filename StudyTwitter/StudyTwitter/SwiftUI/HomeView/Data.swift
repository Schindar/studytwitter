//
//  Data.swift
//  StudyTwitter
//
//  Created by admin on 10.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import Foundation

// load the data from the homes.json file and assign it to the homeData.
let homeData:[Home] = load("homes.json")

//  - description:This function load the home data from the files homes.json.
//  - parameters:
//   - filename: The name of the file.
//   - T: Is the class self.
//  - throws: If file not load or parse error.
//  - returns: The data that are defined in the homes.json file.
func load<T:Decodable>(_ filename:String, as type:T.Type = T.self) -> T{
    let data:Data
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }
    
    do {
        data = try Data(contentsOf: file)
    } catch {
       fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }
    
    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
