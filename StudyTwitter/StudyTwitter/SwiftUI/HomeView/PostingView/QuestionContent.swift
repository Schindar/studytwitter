
import SwiftUI
import SDWebImageSwiftUI
import Firebase

struct QuestionContent : View {
    
    var user = ""
    var image = ""
    var id = ""
    var likes = ""
    var postedDate = ""
    var msg = ""
    @State private var comment: String = ""
    @State private var show_modal = false
    @State private var showingDeletionAlert = false
    
    
    var body : some View{
        
        VStack(alignment: .leading, content: {
            Color.black.edgesIgnoringSafeArea(.bottom)
            HStack{
                
                Voting(user: self.user, image: self.image, id: self.id, likes: self.likes, postedDate: self.postedDate, msg: self.msg)
                HStack(alignment: .top){
                    VStack(alignment: .leading){
                        Button(action: {
                            print("go to the user profil")
                        }) {
                            Text(user).fontWeight(.heavy)
                        }
                        VStack(alignment: .leading){
                            Text(self.postedDate)
                                .background(Color.gray)
                            
                        }
                        Text(msg).padding(.top, 8)
                        Button(action: {
                            print("go to the hashtags view")
                        }) {
                            Text(msg.findHashtags().joined(separator: " ").lowercased()).background(Color("bg"))
                                .foregroundColor(.white)
                                .clipShape(Capsule())
                        }
                        
                    }
                }
                Spacer()
                
            }
            if self.image != ""{
                AnimatedImage(url: URL(string: image)).resizable().frame(height: 250)
            }
            
            
     CommentContent(UserName: self.user,msg: self.msg).environmentObject(observerComments())
            
        }
        
        )
    }
}
struct QuestionContent_Previews: PreviewProvider {
    static var previews: some View {
        QuestionContent()
    }
}

