//
//  ChatView.swift
//  StudyTwitter
//
//  Created by admin on 05.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
//
//  ChatView.swift
//  PostingStudy
//
//  Created by admin on 03.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct ChatView: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor.systemBlue
    }
    
    @State var selected = 0
    
    var body: some View {
        
        Group{
            if(self.session.session != nil){
                
                TabView(selection: $selected) {
                    ChatsView().tabItem({
                        Image(systemName: ChatConstants.TabBarImageName.tabBar0)
                            .font(.title)
                        Text("\(ChatConstants.TabBarText.tabBar0)")
                    }).tag(0)
                    ChatGroupsView().tabItem({
                        Image(systemName: ChatConstants.TabBarImageName.tabBar1)
                            .font(.title)
                        Text("\(ChatConstants.TabBarText.tabBar1)")
                    }).tag(1)
                    Contacts().tabItem({
                        Image(systemName: ChatConstants.TabBarImageName.tabBar2)
                            .font(.title)
                        Text("\(ChatConstants.TabBarText.tabBar2)")
                    }).tag(2)
                }.accentColor(Color.red)
            }else{
                AuthView()
            }
        }.onAppear(perform: self.getUser)
    }
    
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        ChatView().environmentObject(SessionStore())
    }
}
