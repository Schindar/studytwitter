//
//  GroupChatingChatView.swift
//  StudyTwitter
//
//  Created by admin on 06.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI
import Firebase

struct GroupChatingChatView: View {
    
    @EnvironmentObject var session: SessionStore
    var chatGroup: ChatGroup
       @State private var message = ""
       @State private var showCancelButton: Bool = false
       func getUser(){
           self.session.listen()
       }
    var body: some View {
        Group{
            if(self.session.session != nil){
               VStack {
                               // I've removed the text line from here and replaced it with a list
                               // List is the way you should create any list in SwiftUI
                               List {
                                  Text("Hello")
                               }
                               
                               // TextField are aligned with the Send Button in the same line so we put them in HStack
                               HStack {
                                   // this textField generates the value for the composedMessage @State var
                                 
                                 TextField("Message...",text: $message, onEditingChanged: { isEditing in
                                     self.showCancelButton = true
                                 }, onCommit: {
                                     print("onCommit")
                                 }).foregroundColor(.black).lineSpacing(30)
                                 
                                 Button(action: {
                                     self.message=""
                                 })
                                 {
                                     Image(systemName: "xmark.circle.fill").opacity(message == "" ? 0 : 1)
                                 }
                                   // the button triggers the sendMessage() function written in the end of current View
                                  
                               // that's the height of the HStack
                                 Button(action: {
                                     
                                 if self.message.isEmpty == false {
                                     
                                    
                                    let senderId = self.session.session?.uid
                                    let senderFirstName = self.session.session?.firstName
                                    let senderLastName = self.session.session?.lastName
                                    let senderName = (senderFirstName ?? "" ) + "" + (senderLastName ?? "")
                                    
                                    let newMessage:String = self.message
                                    createMessage(newMessage1: newMessage , chatGroup: self.chatGroup, senderName1 : senderName,senderID1: senderId! )
                                     self.message = ""
                                 
                                    }
                                     
                                 }) {
                                     Text("Send")
                                 }.padding(.all)
                                     .background(Color.green)
                                     .cornerRadius(16)
                                     .foregroundColor(.white)
                                     .font(Font.body.bold())
                           }
                             }
                
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

/*
struct GroupChatingChatView_Previews: PreviewProvider {
    static var previews: some View {
        GroupChatingChatView()
    }
}
*/

func createMessage(newMessage1 : String, chatGroup : ChatGroup, senderName1 : String, senderID1 : String){
    
    let db =  Firestore.firestore()
    let thisChatGroup = db.collection("ChatGroups").document(chatGroup.id).collection("messages").document()
    let jetzt = Date()
    let formater = DateFormatter()
    formater.dateFormat = "yyyy-MM-dd H:mm"
    
    thisChatGroup.setData(["id": thisChatGroup.documentID, "messageText" : newMessage1, "senderId": senderID1, "senderName" : senderName1, "datum" : formater.string(from: jetzt) ]){ (err) in
        if err != nil{
            print((err?.localizedDescription)!)
            return
        }
        print("Succesed")
    }
    
    
    }

