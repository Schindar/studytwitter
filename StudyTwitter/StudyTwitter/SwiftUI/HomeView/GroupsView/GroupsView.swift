//
//  GroupsView.swift
//  StudyTwitter
//
//  Created by admin on 13.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
struct GroupsView: View {
    @EnvironmentObject var session: SessionStore
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        
        HStack{
          
            Text("Lectures")
                .font(.system(size: 20))
                .fontWeight(.heavy)
                .padding(.leading, 50)
                .foregroundColor(.white)
            
            ScrollView(.horizontal, showsIndicators: false){
                HStack(alignment: .top){
                    ForEach(groups){
                        item in
                        NavigationLink(destination: PostingContentView(shortCut: item.shortCut)){
                            CardView(title: item.groupName, imageName: item.imageName).padding()
                        }
                    }
                }.padding()
            }
            
        }.background( RadialGradient(gradient: Gradient(colors: [Color.blue, Color.black]), center: .center, startRadius: 5, endRadius: 600).edgesIgnoringSafeArea(.all))
    }
}

struct GroupsView_Previews: PreviewProvider {
    static var previews: some View {
        GroupsView().environmentObject(SessionStore())
    }
    
}

