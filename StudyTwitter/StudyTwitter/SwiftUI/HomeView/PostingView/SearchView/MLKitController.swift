//
//  MLKitController.swift
//  StudyTwitter
//
//  Created by admin on 10.01.20.
//  Copyright © 2020 admin. All rights reserved.
//

class MLKitControll{
    
    
    
    func initMLKit(mlKitFeature: String){
        switch mlKitFeature {
        case MLKitConstants.TEXT_RECOGNITION:
            print(MLKitConstants.TEXT_RECOGNITION)
        case MLKitConstants.FACE_DETECTION:
            print(MLKitConstants.FACE_DETECTION)
        case MLKitConstants.BARCODE_SCANNING:
            print(MLKitConstants.BARCODE_SCANNING)
        case MLKitConstants.IMAGE_LABELING:
            print(MLKitConstants.IMAGE_LABELING)
        case MLKitConstants.OBJECT_DETECTION_TRACKING:
            print(MLKitConstants.OBJECT_DETECTION_TRACKING)
        case MLKitConstants.LANDMARK_RECOGNITION:
            print(MLKitConstants.LANDMARK_RECOGNITION)
        case MLKitConstants.LANGUAGE_IDENTIFICATION:
            print(MLKitConstants.LANGUAGE_IDENTIFICATION)
        case MLKitConstants.TRANSLATION:
            print(MLKitConstants.TRANSLATION)
        case MLKitConstants.SMART_REPLAY:
            print(MLKitConstants.SMART_REPLAY)
        case MLKitConstants.AUTOML_MODEL_INFERENCE:
            print(MLKitConstants.AUTOML_MODEL_INFERENCE)
        default:
            print("ML kit feature not exits")
        }
    }
    
}
