//
//  ChatGroupsList.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct ChatGroupsList: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        Group{
            if(self.session.session != nil){
                Text("HALLO")
                
                /*List{
                 
                 }*/
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

struct ChatGroupsList_Previews: PreviewProvider {
    static var previews: some View {
        ChatGroupsList().environmentObject(SessionStore())
    }
}
