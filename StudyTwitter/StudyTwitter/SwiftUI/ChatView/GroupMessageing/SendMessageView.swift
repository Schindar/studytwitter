//
//  SendMessageView.swift
//  StudyTwitter
//
//  Created by admin on 06.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct SendMessageView: View {
    
    @EnvironmentObject var session: SessionStore
    @State private var message = ""
       @State private var showCancelButton: Bool = false
    func getUser(){
              self.session.listen()
          }
    
    var body: some View {
        
        Group{
                if(self.session.session != nil){
                  HStack{
                          
                          TextField("Message...",text: $message, onEditingChanged: { isEditing in
                              self.showCancelButton = true
                          }, onCommit: {
                              print("onCommit")
                          }).foregroundColor(.black).lineSpacing(30)
                          
                          Button(action: {
                              self.message=""
                          })
                          {
                              Image(systemName: "xmark.circle.fill").opacity(message == "" ? 0 : 1)
                          }
                          
                          Button(action: {
                              
                              
                              
                              
                          }) {
                              Text("Send")
                          }.padding(.all)
                              .background(Color.green)
                              .cornerRadius(16)
                              .foregroundColor(.white)
                              .font(Font.body.bold())
                          
                          
                          
                      }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 55, alignment: .topLeading)
                      
                  
    
                }else {
                    AuthView()
                }
        
            }.onAppear(perform: getUser)
        }
    
}

struct SendMessageView_Previews: PreviewProvider {
    static var previews: some View {
        SendMessageView()
    }
}
