//
//  MenuView.swift
//  StudyTwitter
//
//  Created by admin on 07.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct MenuView: View {
    var body: some View {
        VStack(alignment: .leading){
            
            NavigationLink(destination: TextRecognition()){
                MenuItemView(iconName: "person", textString: "Text recognition", cGFloat: 100)
            }
            
            NavigationLink(destination: FaceDetection()){
                MenuItemView(iconName: "faceid", textString: "Face detection", cGFloat: 10)
            }
            
            NavigationLink(destination: BarcodeScanning()){
                MenuItemView(iconName: "person", textString: "Barcode scanning", cGFloat: 10)
            }
            NavigationLink(destination: ImageLabeling()){
                MenuItemView(iconName: "livephoto", textString: "Image labeling", cGFloat: 10)
            }
            NavigationLink(destination: ObjectDetectionTracking()){
                MenuItemView(iconName: "person", textString: "Object detection & tracking", cGFloat: 10)
            }
            NavigationLink(destination: LandmarkRecognition()){
                MenuItemView(iconName: "person", textString: "Landmark recognition", cGFloat: 10)
            }
            NavigationLink(destination: LanguageIdentification()){
                MenuItemView(iconName: "person", textString: "Lanuage identification", cGFloat: 10)
            }
            NavigationLink(destination: Translation()){
                MenuItemView(iconName: "person", textString: "Translation", cGFloat: 10)
            }
            NavigationLink(destination: SmartReplay()){
                MenuItemView(iconName: "person", textString: "Smart Replay", cGFloat: 10)
            }
            NavigationLink(destination: AutoMLModelInference()){
                MenuItemView(iconName: "person", textString: "AutoML model inference", cGFloat: 10)
            }
        }.padding()
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .frame(alignment: .leading)
            .background(   RadialGradient(gradient: Gradient(colors: [Color.black, Color.blue]), center: .center, startRadius: 5, endRadius: 600)).edgesIgnoringSafeArea(.all)
    }
    
}


struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}

