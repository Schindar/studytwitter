//
//  HomeItem.swift
//  StudyTwitter
//
//  Created by admin on 10.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

// will be the item in the homw view.
struct HomeItem: View {
    
    var home:Home

    var body: some View {
        VStack(alignment: .leading, spacing: 16.0){
            Image(home.imageName)
                 .resizable()
                .renderingMode(.original)
                     .aspectRatio(contentMode: .fill)
                 .frame(width: 300, height: 170)
                 .cornerRadius(10)
                 .shadow(radius: 10)
            VStack(alignment: .leading, spacing: 5.0){
                Text(home.name)
                    .font(.headline)
                    .foregroundColor(.primary)
                Text(home.description)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .multilineTextAlignment(.leading)
                .lineLimit(2)
                    .frame(height:40)
            }
        }
    }
}

struct HomeItem_Previews: PreviewProvider {
    static var previews: some View {
        HomeItem(home: homeData[0])
    }
}
