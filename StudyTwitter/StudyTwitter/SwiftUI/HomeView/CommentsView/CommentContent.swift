//
//  CommentContent.swift
//  StudyTwitter
//
//  Created by Ali on 13.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
import SwiftUI
import Firebase
struct CommentContent: View {
    var userId = ""
    var UserName = ""
    @State var typedcomment = ""
    @State var msg = ""
    var image = ""
    var likes = ""
    var id = ""
    var postedDate = ""
    @State private var showingCommentAlert = false
    @State var edit = false
    @State var show = false
    @EnvironmentObject var comment : observerComments
    var body : some View{
        ZStack{
            Color.black.edgesIgnoringSafeArea(.bottom)
            VStack{
                VStack(alignment: .leading){
                    CommentCardView(userId:self.userId,user:self.UserName,image:self.image,id :self.id, likes: self.likes, postedDate: self.postedDate,msg: self.msg)
                }
                .background(Color.white)
                Spacer()
                TextField("Comments" , text:$typedcomment).textFieldStyle(RoundedBorderTextFieldStyle())
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 10){
                        ForEach(self.comment.comments.reversed()){
                            i in
                            CellView(edit: self.edit, data: i)
                        }
                    }
                  
                }
                .padding()
                .padding(.top, 15)
            }
        }
        .navigationBarItems(trailing:
                            HStack {
                                Button(action: {
                                    self.edit.toggle()
                                })
                                {
                                    Text(self.edit ? "Done" : "Edit").padding()
                                }
                                Button(action:{
                                    if  self.typedcomment.trimmingCharacters(in: .whitespaces).isEmpty{
                                        self.showingCommentAlert = true
                                    }
                                    else{
                                        CommentsUtilities.setCommentData(comment: self.typedcomment,msg: self.msg, postedDate: self.postedDate,userId: self.userId)
                                        self.typedcomment = ""
                                    }
                                })
                                {
                                    Image(systemName: "paperplane").resizable().frame(width: 25, height: 25).padding()
                                }
                                .alert(isPresented: $showingCommentAlert) {
                                    Alert(title: Text("Important message"), message: Text("Empty comment is not allowed"), dismissButton: .default(Text("Got it!")))
                                }
                            })
    }

}

