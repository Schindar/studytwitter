//
//  HomeView.swift
//  PostingStudy
//
//  Created by admin on 01.12.19.
//  Copyright © 2019 admin. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
    @EnvironmentObject var session: SessionStore
    @State var flipped = false
    @State var zoomImage = false
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        NavigationView{
            Group{
                if(self.session.session != nil){
                    VStack{
                        HStack{
                            Spacer()
                        }
                        
                        VStack{
                            Text("Account")
                                .fontWeight(.light)
                                .frame(alignment: .topLeading)
                                .foregroundColor(Color.white)
                            NavigationLink(destination: AccountView()){
                                
                                Image("accSetIcon")
                                    .resizable()
                                    .renderingMode(.original)
                                    .aspectRatio(contentMode: .fit)
                                    .foregroundColor(.blue )
                                    .frame(width: 200, height: 100)
                                    .cornerRadius(10)
                                    .shadow(radius: 10)
                                
                            }.frame(width:  self.zoomImage ? 500 : 200,
                                    height: self.zoomImage ? 450  : 100).onTapGesture{
                                        withAnimation {
                                            self.zoomImage.toggle()
                                        }
                            }
                        }
                        
                        
                        VStack{
                            Text("Chat")
                                .fontWeight(.light)
                                .frame(alignment: .topLeading)
                                .foregroundColor(Color.white)
                            
                            NavigationLink(destination: ChatView()){
                                
                                Image("chatIcon")
                                    .resizable()
                                    .renderingMode(.original)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 200, height: 100)
                                    .cornerRadius(10)
                                    .shadow(radius: 10)
                                
                            }
                        }
                        
                        
                        
                        VStack{
                            Text("Group posting")
                                .fontWeight(.light)
                                .frame(alignment: .topLeading)
                                .foregroundColor(Color.white)
                            
                            NavigationLink(destination: GroupsView()){
                                
                                Image("manyPeopleIcon")
                                    .resizable()
                                    .renderingMode(.original)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 200, height: 100)
                                    .cornerRadius(10)
                                    .shadow(radius: 10)
                            }
                        }
                        
                        VStack{
                            Text("Searching")
                                .fontWeight(.light)
                                .foregroundColor(.white)
                                .frame(alignment: .topLeading)
                                .foregroundColor(Color.white)
                            
                            NavigationLink(destination: SearchView()){
                                
                                Image("AIIcont")
                                    .resizable()
                                    .renderingMode(.original)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 200, height: 100)
                                    .cornerRadius(10)
                                    .shadow(radius: 10)
                                
                            }
                        }
                        Spacer()
                        HStack{
                            Button(action: self.session.signOut ){
                                Text("Sign Out")
                                    .fontWeight(.light)
                                    .font(.title)
                                    .foregroundColor(Color.white)
                            }
                        }.frame(height: 50, alignment: .bottom)
                    }
                } else {
                    AuthView()
                }
            }.onAppear(perform: self.getUser)
                .background( RadialGradient(gradient: Gradient(colors: [Color.blue, Color.black]), center: .center, startRadius: 5, endRadius: 600).edgesIgnoringSafeArea(.all))
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environmentObject(SessionStore())
    }
}



