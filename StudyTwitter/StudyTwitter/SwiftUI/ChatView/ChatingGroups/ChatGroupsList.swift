//
//  ChatGroupsList.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI
import FirebaseFirestore


struct ChatGroupsList: View {
    
    @EnvironmentObject var session: SessionStore
    @ObservedObject var datas = observer()
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        Group{
            if(self.session.session != nil){
                List{
                    ForEach(datas.data){i  in
                        HStack{
                            NavigationLink(destination: GroupChatingChatView(chatGroup:i)){
                                Text(i.roomName)
                            }
                            
                            
                        }
                    }
                    
                    /*List{
                     
                     }*/
                }
            }
            else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

struct ChatGroupsList_Previews: PreviewProvider {
    static var previews: some View {
        ChatGroupsList().environmentObject(SessionStore())
    }
}


class observer : ObservableObject{
    
    @Published var data = [ChatGroup]()
    
    init() {
        
        let db = Firestore.firestore().collection("ChatGroups")
        
        db.addSnapshotListener{(snap, err) in
            if err != nil{
                
                print((err?.localizedDescription)!)
                return
            }
            
            for i in snap!.documentChanges{
                
                if i.type == .added{
                    
                    let groupData = ChatGroup(id: i.document.documentID, roomName: i.document.get("roomName") as! String)
                    
                    self.data.append(groupData)
                }
                
            }
        }
    }
}
