//
//  CommentsOvservable.swift
//  StudyTwitter
//
//  Created by Ali on 18.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
import Firebase
/**
Observation of changes on  comments in *Firestore*
*/
class observerComments: ObservableObject{
    @Published var comments = [datatypeOfComments]()
    init(userId : String,postedDate:String) {
        let db = Firestore.firestore()
        db.collection("comments").whereField("id", isEqualTo: userId).whereField("postedDate", isEqualTo: postedDate).addSnapshotListener{
            (snap,err) in
            if err != nil{
                print((err?.localizedDescription)!)
                return
            }
            for i in snap!.documentChanges{
                if i.type == .added{
                    let commentator = i.document.get("commentator") as! String
                    let comment = i.document.get("comment") as! String
                    let id = i.document.documentID
                    let msg = i.document.get("msg") as! String
                    let postedDate = i.document.get("postedDate") as! String
                    let userId = i.document.get("userId") as! String
                    self.comments.append(datatypeOfComments(id:id,msg:msg, commentator: commentator,comment:comment, userId: userId, postedDate: postedDate))
                }
                if i.type == .removed{
                    let id = i.document.documentID
                    for j in 0..<self.comments.count{
                        if self.comments[j].id == id{
                            self.comments.remove(at: j)
                            return
                        }
                    }
                }
            }
        }
    }

}

