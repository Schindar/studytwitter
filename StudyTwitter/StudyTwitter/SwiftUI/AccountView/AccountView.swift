//
//  AccountView.swift
//  PostingStudy
//
//  Created by admin on 01.12.19.
//  Copyright © 2019 admin. All rights reserved.
//

import SwiftUI

struct AccountView: View {
    
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var email: String = ""
    @State var password: String = ""
    @State var conformPassword: String = ""
    @State var error: String = ""
    @State var image: Image? = nil
    @State var showingPicker = false
    @State var isCancelled = false
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        
        NavigationView{
            Group{
                if(self.session.session != nil){
                    
                    GeometryReader{ geometry in
                        VStack{
                                Button(action: {
                                    self.showingPicker = true
                                }) {
                                    
                                    Image(systemName: Constants.personCircleFill)
                                        .imageScale(.large)
                                        .foregroundColor(.blue)
                                         .frame(width: 150, height: 150)
                                    
                                }
                                
                                self.image?
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: 150, height: 150)
                                    .clipped()
                                    .cornerRadius(150)
                                    .padding(.bottom, 5)
                                
                                TextField("First name", text: self.$firstName)
                                    .font(.system(size: 14))
                                    .padding(12)
                                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                                    .foregroundColor(Color.white)
                                
                                TextField("Last name", text: self.$lastName)
                                    .font(.system(size: 14))
                                    .padding(12)
                                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                                    .foregroundColor(Color.white)
                                
                                
                                TextField("Email adress", text: self.$email)
                                    .font(.system(size: 14))
                                    .padding(12)
                                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                                    .foregroundColor(Color.white)
                                
                                
                                
                                SecureField("Password", text: self.$password)
                                    .font(.system(size: 14))
                                    .padding(12)
                                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color.green, lineWidth: 1))
                                    .foregroundColor(Color.white)
                                
                            }.padding(.vertical, 64)
                                .sheet(isPresented: self.$showingPicker,
                                       onDismiss: {
                                        // do whatever you need here
                                }, content: {
                                    ImagePicker.shared.view
                                })
                                .onReceive(ImagePicker.shared.$image) { image in
                                    // This gets called when the image is picked.
                                    // sheet/onDismiss gets called when the picker completely leaves the screen
                                    self.image = image
                            }
                            
                            if (self.error != ""){
                                Text(self.error)
                                    .font(.system(size: 14, weight: .semibold))
                                    .foregroundColor(.white)
                                    .padding()
                            }
                            
                            Spacer()
                    
                    }.padding(.horizontal, 10)

                } else {
                    AuthView()
                }
            }.onAppear(perform: self.getUser)
            
            
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView().environmentObject(SessionStore())
    }
}
