//
//  CardView.swift
//  StudyTwitter
//
//  Created by admin on 05.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
struct CardView: View {
    let title: String
    let imageName: String
    var body: some View {
        VStack {
            
            Text(title)
            .font(.subheadline)
            .bold()
            .foregroundColor(.white)
            .frame(alignment: .center)
            .padding()
            
            Image(imageName)
            .resizable()
            .renderingMode(.original)
            .aspectRatio(contentMode: .fill)
            .cornerRadius(10)
            .frame(width: 200.0, height: 400, alignment: .center)
            //.padding()
            .cornerRadius(20)
            .shadow(radius: 20)
        }
        
    }

}
struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(title: "", imageName: "")
    }

}

