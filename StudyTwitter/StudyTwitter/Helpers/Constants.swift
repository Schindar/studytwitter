//
//  Constants.swift
//  StudyTwitter
//
//  Created by admin on 08.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import Foundation


struct Constants {
    
    static let upLoadButtonImage = "square.and.arrow.up"
    static let personCircleFill = "person.circle.fill"
    
    struct Storyboard {
        static let homeViewController = "HomeVC"
        static let accountView = "AccountView"
        static let searchView = "SearchView"
        static let groupsView = "GroupsView"
        static let chatView = "ChatView"
    }
}
