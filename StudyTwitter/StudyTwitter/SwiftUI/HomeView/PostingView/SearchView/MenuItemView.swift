//
//  MenuItemView.swift
//  StudyTwitter
//
//  Created by admin on 07.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct MenuItemView: View {
    
    var iconName: String
    var textString: String
    var cGFloat:CGFloat
    
    var body: some View {
        HStack{
            Image(systemName: self.iconName)
                .foregroundColor(.white)
                .imageScale(.large)
            Text(self.textString)
                .foregroundColor(.white)
                .font(.headline)
        }.padding(.top, cGFloat)
        
        
    }
}

struct MenuItemView_Previews: PreviewProvider {
    static var previews: some View {
        MenuItemView(iconName: "", textString: "", cGFloat: 0)
    }
}
