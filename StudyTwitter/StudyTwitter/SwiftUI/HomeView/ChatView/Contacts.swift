//
//  Contacts.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI
import Contacts
import ContactsUI


struct Contacts: View{
   
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    func fetchContacts(){
        print("Loading Contacts")
        let store = CNContactStore()
        store.requestAccess(for: .contacts) {  (granted, err) in
            if let err = err{
                print("failed to request access" , err)
                return
            }
            
            if granted{
                print ("Access granted")
                
                 let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do{
                    var contactList = [ContactList]()
                    
                    try store.enumerateContacts(with: request, usingBlock:  {(contact, stopPointerIfYouWantToStopEnumerating) in
                    
                        print(contact.givenName)
                        print(contact.familyName)
                    print(contact.phoneNumbers.first?.value.stringValue ?? "")
                        //let gname = contact.givenName
                        //let fname = contact.familyName
                        let phonenumber = (contact.phoneNumbers.first?.value.stringValue ?? "")
                        
                    contactList.append(ContactList(name:contact.givenName + " " + contact.familyName + " " + phonenumber, hasFavourited: false))
                        
                        print (contactList)
                    })
                    
                  
                } catch let err {
                    print ("Failed to enumerate contacts: ", err)
                }
                
               
                
            }else {
                print ("Access denied")
            }
        }
    }
    
    
    
    
    var body: some View {
        
        Group{
            if(self.session.session != nil){
                VStack {
                    Text("Contacts")
                        .font(.largeTitle)
                        .fontWeight(.heavy)
                    
                        .onAppear(perform: fetchContacts);
                    
                    
                    
                    List{
                        Text("John Doe")
                           Text("Append array here!")
                            .font(.subheadline)
                        }
                            
                            
                        
                        
                    
                    // Creating a mutable object to add to the contact
                    //let contact = CNMutableContact()
                     
                    //contact.imageData = NSData() // The profile picture as a NSData object
                     
                    //contact.givenName = "John"
                   // contact.familyName = "Appleseed"
                    
                }.padding(.horizontal, 20)
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

struct Contacts_Previews: PreviewProvider {
    static var previews: some View {
        Contacts().environmentObject(SessionStore())
    }
}
