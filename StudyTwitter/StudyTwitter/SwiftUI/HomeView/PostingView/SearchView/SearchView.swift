//
//  SearchView.swift
//  StudyTwitter
//
//  Created by admin on 13.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI


struct SearchView: View {
    
    @EnvironmentObject var session: SessionStore
    @State var showMenuBar = false
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        
        let drag = DragGesture()
            .onEnded{
                if $0.translation.width < -100 {
                    withAnimation{
                        self.showMenuBar = false
                    }
                }
        }
        return NavigationView{
            GeometryReader{ geometry in
                Group{
                    if(self.session.session != nil){
                        ZStack(alignment: .leading){
                            ContentSearchView(showMenuBar: self.$showMenuBar)
                                .frame(width: geometry.size.width, height: geometry.size.height)
                                .offset(x: self.showMenuBar ? geometry.size.width/2: 0)
                                .disabled(self.showMenuBar ? true : false)
                            if self.showMenuBar {
                                MenuView().frame(width: geometry.size.width/2)
                                    .transition(.move(edge: .leading))
                            }
                        }.gesture(drag)
                    }else{
                        AuthView()
                    }
                }.onAppear(perform: self.getUser)
            }
            .navigationBarTitle("Artificial intelligence", displayMode: .inline)
            .navigationBarItems(leading: (
                Button(action: {
                    withAnimation{
                        self.showMenuBar.toggle()
                    }
                }) {
                    Image(systemName: "line.horizontal.3")
                        .imageScale(.large)
                }
            ))
        }
    }
}



struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}

struct ContentSearchView: View{
    @Binding var showMenuBar: Bool
    
    var body: some View {
       ZStack{
                 RadialGradient(gradient: Gradient(colors: [Color.black, Color.blue]), center: .center, startRadius: 5, endRadius: 600)
                 Button(action:{
                     withAnimation{
                         self.showMenuBar = true
                     }
                 }){
                     BreathingAnimation(color: Color.blue)
                 }
             }.edgesIgnoringSafeArea(.all)
    }
}
