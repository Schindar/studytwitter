//
//  Home.swift
//  StudyTwitter
//
//  Created by admin on 10.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

// Will use for the homes.json file. It assign all value that are
// defined in this struct.
struct Home: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var imageName: String
    var category: Category
    var description: String
    var view: String
    
    
    /// Allow the user to difference what is private and public in the home view.
    enum Category: String, CaseIterable, Codable, Hashable {
        case intern = "privat"
        case extern = "public"
    }
}
