//
//  Rounded.swift
//  StudyTwitter
//
//  Created by Ali on 15.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
struct Rounded : Shape {
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 25, height: 25))
        return Path(path.cgPath)
    }

}

