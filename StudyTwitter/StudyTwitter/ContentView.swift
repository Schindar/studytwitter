//
//  ContentView.swift
//  StudyTwitter
//
//  Created by admin on 10.01.20.
//  Copyright © 2020 admin. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        Group{
            if(self.session.session != nil){
                HomeView()
            } else {
                AuthView()
            }
        }.onAppear(perform: getUser)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(SessionStore())
    }
}
