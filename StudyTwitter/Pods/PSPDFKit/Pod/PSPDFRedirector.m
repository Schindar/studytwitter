//
//  main.m
//  PSPDFRedirector
//
//  Copyright (c) 2015 PSPDFKit GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

// Required for PSPDFKitRedirectNotification to load.
@interface PSPDFKitRedirector : NSObject @end
@implementation PSPDFKitRedirector @end

__attribute__((constructor)) static void PSPDFKitRedirectNotification() {
    NSString *message = @"The PSPDFKit Cocoapod requires an API key. Learn more at https://pspdfkit.com/cocoapods.";
    NSLog(@"Warning: %@", message);
    [NSNotificationCenter.defaultCenter addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:nil usingBlock:^(__unused NSNotification *note) {
        [[[UIAlertView alloc] initWithTitle:@"PSPDFKit" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil] show];
    }];
}