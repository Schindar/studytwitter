import SwiftUI
import SDWebImageSwiftUI
import Firebase
struct CommentCardView : View {
    var userId = ""
    var user = ""
    var image = ""
    var id = ""
    var likes = ""
    var postedDate = ""
    var msg = ""
    @State private var show_UserProfil = false
    var body : some View{
        HStack(alignment: .top){
            VStack{
                Voting(user: self.user, image: self.image, id: self.id, likes: self.likes, postedDate: self.postedDate, msg: self.msg)
            }
            VStack(alignment: .leading){
                Button(action: {
                    print("go to the user profil")
                    self.show_UserProfil.toggle()
                })
                {
                    Text(user).fontWeight(.heavy).background(Color.white)
                }
                .sheet(isPresented: self.$show_UserProfil) {
                    UserAccountView(user:self.user).environmentObject(UsersPostsObservable(userId: self.userId))
                }
                Text(self.postedDate).font(.subheadline).foregroundColor(.gray)
                Text(msg).padding(.top, 8).background(Color.white)
                if self.image != ""{
                    AnimatedImage(url: URL(string: image)).resizable().frame(height: 250)
                }
            }
        }
        .padding()
    }

}

