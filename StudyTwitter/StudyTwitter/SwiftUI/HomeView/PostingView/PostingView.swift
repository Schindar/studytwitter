//
//  ContentDetail.swift
//  StudyTwitter
//
//  Created by Schindar on 14.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
import Firebase
struct PostView: View {
    @EnvironmentObject var observedData : getPostData
    var shortcut = ""
    var searchText = ""
    var body : some View{
   
            List(observedData.datas.filter{
                $0.msg.lowercased().contains(searchText.lowercased()) || searchText.lowercased() == ""
            }
                 .sorted(by: {
                     $0.postedData > $1.postedData
                 }))
            {
                i in
                VStack(alignment: .leading){
                    PostCellCard(userId:i.userId,user: i.name,image: i.pic, id: i.id, likes: i.likes, postedDate: i.postedData,msg: i.msg)
                }
            }.listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
        
      
    }

}
struct PostView_Previews: PreviewProvider {
    static var previews: some View {
        PostView(shortcut :"").environmentObject(getPostData(shortCut: ""))
    }

}

