//
//  AddNewChatView.swift
//  StudyTwitter
//
//  Created by admin on 26.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI




struct AddNewChatView: View {
    
    @State private var chatRoomName = ""
    @State private var showCancelButton: Bool = false
    
    var body: some View {
        
        VStack {
            HStack{
                
                
                //    Image(systemName: "magnifyingglass")
                
                TextField("Chat Room Name",text: $chatRoomName, onEditingChanged: { isEditing in
                    self.showCancelButton = true
                }, onCommit: {
                    print("onCommit")
                }).foregroundColor(.black).lineSpacing(30)
                
                Button(action: {
                    self.chatRoomName=""
                })
                {
                    Image(systemName: "xmark.circle.fill").opacity(chatRoomName == "" ? 0 : 1)
                }
                
                
                Button(action: {
                    
                    
                    
                }) {
                    Text("Create Room")
                }.padding(.all)
                    .background(Color.red)
                    .cornerRadius(16)
                    .foregroundColor(.white)
                    .font(Font.body.bold())
                
                
            }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 200, alignment: .topLeading)
            
        }
    }
}

struct AddNewChatView_Previews: PreviewProvider {
    static var previews: some View {
        AddNewChatView()
    }
}
