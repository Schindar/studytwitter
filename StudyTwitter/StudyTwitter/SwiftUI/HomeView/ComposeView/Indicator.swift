//
//  Indicator.swift
//  StudyTwitter
//
//  Created by Schindar on 04.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import Foundation
import SwiftUI
struct indicator: UIViewRepresentable {
    func makeUIView(context: UIViewRepresentableContext<indicator>) -> UIActivityIndicatorView {
        let indicator1 = UIActivityIndicatorView(style: .large)
        indicator1.startAnimating()
        return indicator1
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<indicator>) {
    }

}

