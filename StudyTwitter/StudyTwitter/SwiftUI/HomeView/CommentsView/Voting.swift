//
//  Voting.swift
//  StudyTwitter
//
//  Created by Ali on 14.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
import Firebase
struct Voting: View {
    @State var upVote = false
    @State var neutral = true
    var user = ""
    var image = ""
    var id = ""
    var likes = ""
    var postedDate = ""
    var msg = ""
    @State private var comment: String = ""
    @State private var show_modal = false
    var body: some View {
        VStack{
            HStack{
                Button(action: {
                    self.upVoting()
                })
                {
                    Image(systemName: "arrowtriangle.up").resizable().frame(width: 15, height: 15)
                }
                .foregroundColor(Color("darkAndWhite"))
            }
            .padding(.top, 8)
            Text("\(likes)").padding(.top, 8)
            HStack{
                Button(action: {
                    self.downVoting()
                })
                {
                    Image(systemName: "arrowtriangle.down").resizable().frame(width: 15, height: 15)
                }
                .foregroundColor(Color("darkAndWhite"))
            }
            .padding(.top, 8)
        }
    }

    func downVoting(){
        let db = Firestore.firestore()
        let like = Int.init(self.likes)!
        if(neutral){
            db.collection("posts").document(self.id).updateData(["likes": "\(like - 1)"]) {
                (err) in
                if err != nil{
                    print((err)!)
                    return
                }
                print("down updated....")
            }
            self.neutral = false
            self.upVote = true
        }
        else if(!upVote){
            db.collection("posts").document(self.id).updateData(["likes": "\(like - 1)"]) {
                (err) in
                if err != nil{
                    print((err)!)
                    return
                }
                print("down updated....")
            }
            self.neutral = true
        }
    }

    func upVoting(){
        let db = Firestore.firestore()
        let like = Int.init(self.likes)!
        if(neutral ){
            db.collection("posts").document(self.id).updateData(["likes": "\(like + 1)","postedDate":"\(Utilities.getCurrentDate())"]) {
                (err) in
                if err != nil{
                    print((err)!)
                    return
                }
                print("up updated....")
            }
            self.neutral = false
            self.upVote = false
        }
        else if(upVote){
            self.neutral = true
            db.collection("posts").document(self.id).updateData(["likes": "\(like + 1)","postedDate":"\(Utilities.getCurrentDate())"]) {
                (err) in
                if err != nil{
                    print((err)!)
                    return
                }
                print("up updated....")
            }
        }
    }

}
struct Voting_Previews: PreviewProvider {
    static var previews: some View {
        Voting()
    }

}

