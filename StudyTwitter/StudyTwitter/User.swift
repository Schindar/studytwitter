//
//  User.swift
//  PostingStudy
//
//  Created by admin on 30.11.19.
//  Copyright © 2019 admin. All rights reserved.
//

class User {
    var uid: String
    var email: String?
    var firstName: String
    var lastName: String
    
    
    init(uid: String, email: String?, firstName: String, lastName: String){
        self.uid = uid
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
    }
}
