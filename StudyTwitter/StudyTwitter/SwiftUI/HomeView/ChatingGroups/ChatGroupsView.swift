//
//  ChatGroupsView.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct ChatGroupsView: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    
    var body: some View {
        
        Group{
            if(self.session.session != nil){
                VStack{
                    VStack{
                        AddNewChatView()
                    }.frame(minHeight: CGFloat(50)).padding()
                    
                    VStack{
                        ChatGroupsList()
                    }
                }
                
            }else {
                AuthView()
            }
        }.onAppear(perform: getUser)
        
    }
    struct ChatGroupsView_Previews: PreviewProvider {
        static var previews: some View {
            ChatGroupsView().environmentObject(SessionStore())
        }
    }
}

