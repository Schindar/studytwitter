//
//  PostsObervable.swift
//  StudyTwitter
//
//  Created by Ali on 18.12.19.
//  Copyright © 2019 Group8. All rights reserved.
//
import SwiftUI
import Firebase
/**
Observation of changes on  posts in *Firestore*
*/
class getPostData : ObservableObject{
    @Published var datas = [postsDatatype]()
    init(shortCut :String) {
        let db = Firestore.firestore()
        db.collection("posts").whereField("groupsShortcut", isEqualTo: shortCut).addSnapshotListener {
            (snap, err) in
            if err != nil{
                print((err?.localizedDescription)!)
                return
            }
            for i in snap!.documentChanges{
                // If there is any additional information in post document in *FireStore*
                if i.type == .added{
                    let id = i.document.documentID
                    let name = i.document.get("name") as! String
                    let msg = i.document.get("msg") as! String
                    let pic = i.document.get("pic") as! String
                    let likes = i.document.get("likes") as! String
                    let groupsShortcut = i.document.get("groupsShortcut") as! String
                    let postedDate = i.document.get("postedDate") as! String
                    let userId = i.document.get("id") as! String
                    DispatchQueue.main.async {
                        self.datas.append(postsDatatype(id: id, name: name, msg: msg, groupsShortcut: groupsShortcut, likes: likes, postedData: postedDate, userId: userId, pic: pic))
                    }
                }
                // If there is any Modifation on a specific value of post document in *FireStore*
                if i.type == .modified{
                    let id = i.document.documentID
                    let likes = i.document.get("likes") as! String
                    let postedDate = i.document.get("postedDate") as! String
                    for j in 0..<self.datas.count{
                        if self.datas[j].id == id{
                            self.datas[j].postedData = postedDate
                            self.datas[j].likes = likes
                            return
                        }
                    }
                }
                // If there is any documentation removed from the user.
                if i.type == .removed{
                    let id = i.document.documentID
                    for j in 0..<self.datas.count{
                        if self.datas[j].id == id{
                            self.datas.remove(at: j)
                            return
                        }
                    }
                }
            }
        }
    }

}

