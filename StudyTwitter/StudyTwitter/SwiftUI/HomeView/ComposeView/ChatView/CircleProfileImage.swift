//
//  CircleProfileImage.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct CircleProfileImage: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    var body: some View {
        
        NavigationView{
            Group{
                if(self.session.session != nil){
                    
                    Image("person-icon-1675").resizable().frame(width: 300, height: 300)
                        .clipShape(Circle()).overlay(Circle().stroke(Color.white, lineWidth: 2))
                        .shadow(radius: 10)
                    
                }else {
                    AuthView()
                }
            }.onAppear(perform: getUser)
        }
    }
}

struct CircleProfileImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleProfileImage().environmentObject(SessionStore())
    }
}
