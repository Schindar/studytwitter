//
//  ChatProfile.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI
import Firebase



struct ProfileView: View {
    
    @EnvironmentObject var session: SessionStore
    
    func getUser(){
        self.session.listen()
    }
    
    var db: Firestore!
    let dat = Firestore.firestore()
    let databaseRef = Database.database().reference()
    let userID : String =  Auth.auth().currentUser!.uid
    
    
    var body: some View {
        
        Group{
            if(self.session.session != nil){
                
                ZStack {
                    
                    VStack(){
                        ProfileBackground().frame( height: 200)
                        CircleProfileImage().frame(width: 200, height: 200)
                            .offset(y: -130)
                        
                        VStack(){
                            Color.white
                            Text("")
                            Text(userID)
                            
                        }
                        
                    }
                    
                    
                }
                
                
                /*ZStack {
                 Color.white
                 Text(userID)
                 //Text(userID)
                 
                 /*VStack {
                 Rectangle()
                 .frame(width: 120, height: 6)
                 .cornerRadius(3)
                 .opacity(0.3)
                 Spacer()
                 Image(systemName: SFSymbolName.person_fill).font(.title)
                 }
                 
                 }.edgesIgnoringSafeArea(.all)  */
                 }*/
                
            }else{
                AuthView()
            }
            
        }.onAppear(perform: self.getUser)
    }
    /*
     private func getDocument() {
     // [START get_document]
     let docRef = db.collection("users").document(userID)
     
     docRef.getDocument { (document, error) in
     if let document = document, document.exists {
     let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
     print("Document data: \(dataDescription)")
     } else {
     print("Document does not exist")
     }
     }
     // [END get_document]
     }
     */
    struct ProfileView_Previews: PreviewProvider {
        static var previews: some View {
            ProfileView().environmentObject(SessionStore())
        }
    }
    //let database = Firestore.firestore()
    //let databaseRef = Database.database().reference()
    
    // let userID : String = Auth.auth().currentUser!.uid
    // let email :   Auth.auth().currentUser?.firstname
    
    //let user =  databaseRef.child("users").child(String "Auth.auth().currentUser?.uid")
    
    //  let currentUser = Auth.auth().currentUser
    
    //let user = databaseRef.child("users").child(userID)
    
    /*database.collection("users").getDocuments() { (querySnapshot, err) in
     if let err = err {
     print("Error getting documents: \(err)")
     } else {
     for document in querySnapshot!.documents {
     print("\(document.documentID) => \(document.data())")
     }
     }
     }*/
    
    
}




