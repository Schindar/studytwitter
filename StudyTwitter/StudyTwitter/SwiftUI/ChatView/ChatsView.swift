//
//  ChatsView.swift
//  PostingStudy
//
//  Created by admin on 04.12.19.
//  Copyright © 2019 admin. All rights reserved.
//
import SwiftUI

struct ChatsView: View {
    
    @EnvironmentObject var session: SessionStore
    @State var isProfileViewPresented = false
    
    func getUser(){
        self.session.listen()
    }
    
    fileprivate func extractedFunc() -> Image {
        return Image(systemName: SFSymbolName.person_circle_fill)
    }
    var body: some View {
        NavigationView{
            Group{
                if(self.session.session != nil){
                    ZStack {
                        Color.white
                        VStack {
                            Text("ChatViews").foregroundColor(.white)
                        }.navigationBarTitle("", displayMode: .inline)
                            .navigationBarItems(trailing:
                                HStack(spacing: 15) {
                                    Button(action: {
                                        print("Button tapped")
                                        self.isProfileViewPresented = true
                                    }) {
                                        extractedFunc().font(.title)
                                    }.sheet(isPresented: $isProfileViewPresented, content: { ProfileView() }).foregroundColor(.red)
                                }
                        )
                    }.edgesIgnoringSafeArea(.all)
                }else{
                    AuthView()
                }
            }.onAppear(perform: getUser)
        }
    }
    
    struct ChatsView_Previews: PreviewProvider {
        static var previews: some View {
            ChatsView()
        }
    }
}
