//
//  WidgetView.swift
//  StudyTwitter
//
//  Created by admin on 11.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct WidgetView: View {
    var body: some View {
         Text("This is a swiftUI view 👋")
    }
}

struct WidgetView_Previews: PreviewProvider {
    static var previews: some View {
        WidgetView()
    }
}
