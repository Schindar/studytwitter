//
//  HomeDetail.swift
//  StudyTwitter
//
//  Created by admin on 13.11.19.
//  Copyright © 2019 Group8. All rights reserved.
//

import SwiftUI

struct HomeDetail: View {
    
    var home:Home
    
    var body: some View {
        List{
            NavigationView{
            
                if home.view == Constants.Storyboard.accountView {
                     NavigationLink("Go to the account view.", destination: AccountView())
                } else if home.view == Constants.Storyboard.searchView {
                    NavigationLink("Go to the searching view", destination: SearchView())
                } else if home.view == Constants.Storyboard.postingView {
                    NavigationLink("Go to the posting view", destination: PostingView())
                } else if home.view == Constants.Storyboard.chatView {
                NavigationLink("Go to the posting view", destination: ChatView())
            
                }
            }
            .listRowInsets(EdgeInsets())
            VStack{
                Text(home.description)
                    .foregroundColor(.primary)
                    .font(.body)
                .lineLimit(nil)
                .lineSpacing(12)
            }
        }
    }
}

struct HomeDetail_Previews: PreviewProvider {
    static var previews: some View {
        HomeDetail(home: homeData[0])
    }
}
